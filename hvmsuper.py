#!/usr/bin/env python

__version__ = '0.0.0.1.3.70.ma1.2'

import sys
sys.dont_write_bytecode = True

def usage():
    print 'python ' + sys.argv[0] + ' [option]'
    print """\
    options:

        list [hypervisor]
        list-vms
        list-not-running
        list-no-xml
        list-hypervisors
        status [vm|hypervisor]
        migrate <vm> [hypervisor]
        shutdown|destroy <vm> [hypervisor]
        start <vm> [hypervisor]
        start-not-running
        evac <hypervisor> [hypervisor]
        <hypervisor> cmd
        cmdall cmd
        check [dupmac|dupuuid|duplun|dupvm]
        check-ssh <hypervisor>
        show [most-vms|least-vms|hw-mem|mem-free]
        get <hypervisor> [most-mem-used|most-mem-defined
                          most-cpu-used|most-cpu-defined]

        balance [mem|vms|cpu]

        report [hypervisor|type]
            type: cpu|vcpu|hmem|mem|vms-pcpu|vms-pmem

        maintenance <hypervisor> [yum|reboot|rolling-up]

        gen-balance-report

        ------------------
        state - track state change
        alert [cpu]

    """
    return True

import os
import subprocess
import threading
import glob
import string
import smtplib
import json
import time

import config
sendEmail = config.mail['enabled']

def sshCmdStdOut(host=None,cmd=None):
    """ sys stdout
            provides real time subprocess """
    cmdline = str(config.ssh['cmd']) + host + ' ' + str(cmd)
    subcmd = subprocess.Popen(cmdline.split(), stdout=subprocess.PIPE)
    exitcode = subcmd.wait()
    if (exitcode != 0):
        print 'Error exitcode ' + str(exitcode)
    for line in subcmd.stdout:
        #print line
        sys.stdout.write(line)
        sys.stdout.flush()
    return exitcode

def sshCmdDict(host=None,cmd=None):
    """ dict
            return dict """
    cmdline = str(config.ssh['cmd']) + host + ' ' + cmd
    subcmd = subprocess.Popen(cmdline.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, err = subcmd.communicate()
    returnDict = {}
    exitcode = subcmd.wait()
    if (exitcode != 0):
        returnDict[host] = str(exitcode) + ' ' + str(err)
        return returnDict, exitcode
    returnDict[host] = output
    return returnDict, exitcode


def sendMail(subject='',msg=''):
    """ send email """
    SUBJECT = config.mail['subject'] + ': ' + str(subject)
    HOST = config.mail['server']
    PORT = config.mail['port']
    FROM = config.mail['sender']
    TO = config.mail['mailto']

    BODY = string.join((
        "From: %s" % FROM,
        "To: %s" % TO,
        "Subject: %s" % SUBJECT ,
        "",
        msg
        ), "\r\n")

    server = smtplib.SMTP(HOST, PORT)
    server.sendmail(FROM, TO.split(','), BODY)
    server.quit()
    print 'email sent to ' + str(TO)
    return True

def maintenance_hvm_yum(hypervisor):
    """  yum maint """
    print "yum maintenance routine...  "

    # need to run these one at a time...
    cmdline = "yum clean all"
    print str(cmdline)
    exitcode = sshCmdStdOut(host=hypervisor,cmd=cmdline)
    if (exitcode != 0):
        print 'Error exitcode ' + str(exitcode) + ' ' + str (cmdline)
        return exitcode
    print "Done " + str(cmdline)

    cmdline = "bootstrap -o yum"
    print str(cmdline)
    exitcode = sshCmdStdOut(host=hypervisor,cmd=cmdline)
    if (exitcode != 0):
        print 'Error exitcode ' + str(exitcode) + ' ' + str (cmdline)
        return exitcode
    print "Done " + str(cmdline)

    cmdline = "yum update -y"
    print str(cmdline)
    exitcode = sshCmdStdOut(host=hypervisor,cmd=cmdline)
    if (exitcode != 0):
        print 'Error exitcode ' + str(exitcode) + ' ' + str (cmdline)
        return exitcode
    print "Done " + str(cmdline)

    cmdline = "bootstrap -o yum"
    print str(cmdline)
    exitcode = sshCmdStdOut(host=hypervisor,cmd=cmdline)
    if (exitcode != 0):
        print 'Error exitcode ' + str(exitcode) + ' ' + str (cmdline)
        return exitcode
    print "Done " + str(cmdline)

    return exitcode

def check_hvm_ssh(hypervisor):
    """
        ssh $1 uptime
        exitval="$?"

        if [ "$exitval" -ne 0 ]; then
          echo "failnum: $exitval"
        else
          echo "OK"
        fi

     """
    cmdline = "uptime"
    exitcode = sshCmdStdOut(host=hypervisor,cmd=cmdline)
    return exitcode

def check_hvm_ssh_blocking(hypervisor):
    exitcode = 99
    cmdline = "uptime"
    while (exitcode != 0):
        exitcode = check_hvm_ssh(hypervisor)
        print 'check_hvm_ssh_blocking check again in 3...'
        time.sleep(3)
    return exitcode


def maintenance_hvm_reboot(hypervisor):
    """  yum maint """
    print "yum maintenance routine...  "
    #verify no running vms...
    runningList = get_running_list_hvm(hypervisor)
    if runningList:
        for vm in runningList:
            print 'RUNNING ' + str(vm)
            print 'Aborted reboot cmd ' + str(hypervisor)
        exitcode = 99
        return exitcode
    else:
        print "REBOOT"

    cmdline = "/sbin/reboot"
    exitcode = sshCmdStdOut(host=hypervisor,cmd=cmdline)
    if (exitcode != 0):
        print 'Error exitcode ' + str(exitcode) + ' ' + str(cmdline)
        return exitcode
    return exitcode

def get_next_host_up(hypervisor):
    """  get next host in list """
    objects = config.hosts
    somevalue = hypervisor
    previous_ = next_ = None
    l = len(objects)
    for index, obj in enumerate(objects):
        if obj == somevalue:
            if index > 0:
                previous_ = objects[index - 1]
            if index < (l - 1):
                next_ = objects[index + 1]
    return next_

def get_next_host_down(hypervisor):
    """  get next host in list """
    objects = config.hosts
    somevalue = hypervisor
    previous_ = next_ = None
    l = len(objects)
    for index, obj in enumerate(objects):
        if obj == somevalue:
            if index > 0:
                previous_ = objects[index - 1]
            if index < (l - 1):
                next_ = objects[index + 1]
    return previous_

def maintenance_hvm_cycle(hypervisor):

    print 'running maintenance_hvm_yum on ' + hypervisor
    exitcode = maintenance_hvm_yum(hypervisor)
    if (exitcode != 0):
        print 'Error exitcode ' + str(exitcode) + ' ' + str(hypervisor)
        return exitcode

    print 'running maintenance_hvm_reboot on ' + hypervisor
    exitcode = maintenance_hvm_reboot(hypervisor)
    if (exitcode != 0):
        print 'Error exitcode ' + str(exitcode) + ' ' + str(hypervisor)
        return exitcode

    return exitcode

def maintenance_hvm_rolling(hypervisor):

    fullList = config.hosts
    startIndex = fullList.index(hypervisor)
    #print (startIndex)
    rotatedList = fullList[startIndex:] + fullList[:startIndex]

    for hypervisor in rotatedList:
        print str(hypervisor)
        exitcode = maintenance_hvm_routine(hypervisor)
        if (exitcode != 0):
            print 'Exit.Exit! Error exitcode ' + str(exitcode)
            sys.exit(exitcode)

    print 'Done.maintenance_hvm_rolling'
    return exitcode

def maintenance_hvm_routine(hypervisor):

    # check if we need to evac this host...
    runningList = get_running_list_hvm(hypervisor)
    if runningList:
        print '#evac startHypervisor to previousHypervisor'
        startHypervisor = hypervisor
        previousHypervisor = get_next_host_down(hypervisor)
        nextHypervisor = get_next_host_up(hypervisor)
        if previousHypervisor is None:
            previousHypervisor = config.hosts[-1]

        if nextHypervisor is None:
            nextHypervisor = config.hosts[0]

        exitcode = evac_hvm_serial(startHypervisor, previousHypervisor)
        if (exitcode != 0):
            print 'Error evac_hvm_serial ' + str(exitcode)
            sys.exit(exitcode)
        print "Done evac_hvm_serial"

    #exitcode = maintenance_hvm_cycle(hypervisor)
    #if (exitcode != 0):
    #    print 'Error maintenance_hvm_cycle ' + str(exitcode)
    #    sys.exit(exitcode)


#running maintenance_hvm_reboot on vmos-ca5os-03
#yum maintenance routine...
#REBOOT
#Connection to vmos-ca5os-03 closed by remote host.
#Error exitcode 255
#Error exitcode 255 /sbin/reboot
#Error exitcode 255 vmos-ca5os-03
#Error maintenance_hvm_cycle 255
#[krink@mgmtos-ca5-01 hvm-super]$

#running maintenance_hvm_reboot on vmos-ca5os-03
#yum maintenance routine...
#REBOOT
#Connection to vmos-ca5os-03 closed by remote host.
#Error exitcode 255
#Error exitcode 255 /sbin/reboot
#EXITCODE HERE 255
#Error exitcode 255 vmos-ca5os-03
#[krink@mgmtos-ca5-01 hvm-super]$

#    print 'running maintenance_hvm_yum on ' + hypervisor
##    exitcode = maintenance_hvm_yum(hypervisor)
#    if (exitcode != 0):
#        print 'Error exitcode ' + str(exitcode) + ' ' + str(hypervisor)
#        sys.exit(exitcode)


    # need to run these one at a time...
    cmdline = "yum clean all"
    print str(cmdline)
    exitcode = sshCmdStdOut(host=hypervisor,cmd=cmdline)
    if (exitcode != 0):
        print 'Error exitcode ' + str(exitcode) + ' ' + str(cmdline)
        #return exitcode
        sys.exit(exitcode)
    print "Done " + str(cmdline)

    cmdline = "bootstrap -o yum"
    print str(cmdline)
    exitcode = sshCmdStdOut(host=hypervisor,cmd=cmdline)
    if (exitcode != 0):
        print 'Error exitcode ' + str(exitcode) + ' ' + str(cmdline)
        #return exitcode
        sys.exit(exitcode)
    print "Done " + str(cmdline)

    #cmdline = "yum -y update"
    #print str(cmdline)
    #exitcode = sshCmdStdOut(host=hypervisor,cmd=cmdline)
    #if (exitcode != 0):
    #    print 'Error exitcode ' + str(exitcode) + ' ' + str(cmdline)
    #    #return exitcode
    #    sys.exit(exitcode)

    #returnDict instead...
    #maybe put a spinner here...
    print 'returnDict long running proccess...'
    cmdline = "yum -y update"
    print str(cmdline)
    returnDict, exitcode = run_cmd_dict(host=hypervisor, cmd=[cmdline], printout=False)
    if (exitcode != 0):
        print 'Error exitcode returnDict ' + str(exitcode) + ' ' + str(cmdline)
        sys.exit(exitcode)
    print "Done " + str(cmdline)


    cmdline = "bootstrap -o yum"
    print str(cmdline)
    exitcode = sshCmdStdOut(host=hypervisor,cmd=cmdline)
    if (exitcode != 0):
        print 'Error exitcode ' + str(exitcode) + ' ' + str(cmdline)
        #return exitcode
        sys.exit(exitcode)
    print "Done " + str(cmdline)

    print 'running maintenance_hvm_reboot on ' + hypervisor
    exitcode = maintenance_hvm_reboot(hypervisor)
    print "EXITCODE maintenance_hvm_reboot " + str(exitcode)
    if (exitcode != 0) and (exitcode != 255):
        print 'Error exitcode ' + str(exitcode) + ' ' + str(hypervisor) + ' not 0 or 255'
        sys.exit(exitcode)

    print 'must sleep for 15 after successful reboot command'
    print ' otherwise, check_ssh_blocking will be quick enough'
    print ' to get in and out with a successful return  '
    print ' before the system has had a chance to initialize '
    print ' its own shutdown proceedure...'
    time.sleep(15)

    #exitcode = check_hvm_ssh_blocking(hypervisor)
    exitcode = check_ssh_blocking(hypervisor)
    if (exitcode != 0):
        print 'Error exitcode ' + str(exitcode)
        return exitcode
    print "Done check_ssh_blocking"

    #print 'wait another 120...  patience is a virtue'
    #print ' need to sleep for 120 right after the host is back up...'
    #print ' allowing system to get up a few facilities'
    #time.sleep(120)

    print 'wait another 320...  patience is a virtue'
    print ' need to sleep for 320 right after the host is back up...'
    print ' allowing system to get up a few facilities'
    time.sleep(320)

    return exitcode

def maintenance_hvm_cli(hypervisor, maint):
    exitcode = 99

    #validate host
    if hypervisor not in config.hosts:
        print 'hypervisor not found: ' + str(hypervisor)
        exitcode = 1
        sys.exit(exitcode)

    #validate maint
    maint = maint[0]
    maintenance = ['yum', 'reboot', 'rolling-up']
    if maint not in maintenance:
        print 'Not found maint ' + str(maint)
        exitcode = 1
        sys.exit(exitcode)

    if maint == 'yum':
        exitcode = maintenance_hvm_yum(hypervisor)
        sys.exit(exitcode)

    if maint == 'reboot':
        exitcode = maintenance_hvm_reboot(hypervisor)
        sys.exit(exitcode)

    if maint == 'rolling-up':
        exitcode = maintenance_hvm_rolling(hypervisor)
        sys.exit(exitcode)

    print 'DONE maintenance_hvm_cli '
    sys.exit(exitcode)

def run_cmd_cli(host=None, cmd=[]):
    if not cmd:
        cmdline = 'uptime'
    else:
        cmdline = ' '.join(cmd)

    exitcode = sshCmdStdOut(host=host,cmd=cmdline)
    sys.exit(exitcode)

def run_cmd_stdout(host=None, cmd=[]):
    if not cmd:
        cmdline = 'uptime'
    else:
        cmdline = ' '.join(cmd)

    exitcode = sshCmdStdOut(host=host,cmd=cmdline)
    return exitcode


def run_cmd_dict(host='', cmd=[], printout=True):
    """ run command """

    if not cmd:
        cmdline = 'uptime'
    else:
        cmdline = ' '.join(cmd)

    t = threadWithReturn(target=sshCmdDict, args=(host,cmdline))
    t.start()
    ret,e = t.join()

    returnDict = {}
    for key in sorted(ret.iterkeys()):
        returnDict[key] = ret[key]
        #print str(ret[key])
        if printout:
            print str(ret[key])

    #print str(returnDict)
    return returnDict, e

def cmd_all(cmd=[], printout=True):

    if not cmd:
        cmdline ='uptime'
    else:
        cmdline = ' '.join(cmd)

    returnDict = {}
    threads = [threadWithReturn(target=sshCmdDict, args=(host,cmdline)) for host in config.hosts]
    for t in threads: t.start()
    for t in threads:
        ret, e = t.join()
        returnDict.update(ret)
        # perhaps another dict for 'e' as well.

    #print str(thisDict)
    for key in sorted(returnDict.iterkeys()):
        if printout:
            print key
            print returnDict[key]

    return returnDict, e

def list_hypervisors():
    for host in config.hosts:
        print host
    return True

def list_vms(hvm=None):
    """ list vms """
    cmdline = 'virsh -q list --all'
    if hvm:
        host = str(hvm[0])
        run_cmd_dict(host, [cmdline], printout=True)

    if not hvm:
        cmd_all([cmdline], printout=True)

    return True

def list_vms_serial(hvm=None):
    """list vms serial"""
    cmdline = 'virsh -q list --all'
    if not hvm:
        for host in config.hosts:
            print host
            run_cmd_dict(host, [cmdline], printout=True)

    if hvm:
        host = str(hvm[0])
        run_cmd_dict(host, [cmdline], printout=True)

    return True




def list_not_running(checkrunning=True, checkxml=True):

    NotRunningList = []
    NoXMLList = []
    returnList = []

    if sys.argv[1] == "list-not-running":
        checkxml = False

    if sys.argv[1] == "list-no-xml":
        checkrunning = False


    if not os.path.isdir("xml"):
        print 'WARNING!  this script hates not having an xml dir!'

    #print 'gathering all xml...'
    xmlList = []
    xmlFiles = glob.glob("xml/*.xml")
    for file in xmlFiles:
        file = os.path.basename(file)
        xmlList.append(os.path.splitext(file)[0])

    #print 'gathering all running...'
    runningList = get_running_list_all()

    list1 = runningList
    list2 = xmlList

    # NotRunningList
    if checkrunning:
        for item in list2:
            if item not in list1:
                print item
                NotRunningList.append(item)

    if NotRunningList:
        returnList = NotRunningList
        if sendEmail:
            msg = """The hypervisor supervisor has detected the following vms are not running: \n\r"""
            msg += "\n".join(NotRunningList)
            msg += "\n\r Solution: either start the vm or remove the definition"
            sendMail(subject='detected not running', msg=msg)

    #print '--------------------'

    # NoXMLList
    if checkxml:
        for item in list1:
            if item not in list2:
                print item
                NoXMLList.append(item)

    #print str(NoXMLList)
    if NoXMLList:
        returnList = NoXMLList
        if sendEmail:
            msg = """The hypervisor supervisor has detected the following vms are running, but do not have saved xml: \n\r"""
            msg += "\n".join(NoXMLList)
            msg += "\n\r Solution: create server xml definition"
            sendMail(subject='detected not running', msg=msg)

    return returnList


def gen_balance_report(host=''):
    """ generate balance report all nodes """
    # hmem smem list info.all mem.all mem.all.sum pmem.all pmem.all.sum pcpu.all
    print 'gen balance report ' + str(host)
    cmdline = "/opt/vmm/hmem;/opt/vmm/smem;/opt/vmm/list;/opt/vmm/info.all;"
    cmdline += "/opt/vmm/mem.all;/opt/vmm/mem.all.sum;/opt/vmm/pmem.all;"
    cmdline += "/optv/vmm/pmem.all.sum;/opt/vmm/pcpu.all;"

    cmd_all([cmdline])

    return True

def balance_hvms(arg):
    arg = sys.argv[2]

    if arg == 'mem':
        print 'balance ' + str(arg)
        balance_hvms_mem(arg)
        return True

    if arg == 'vms':
        print 'balance ' + str(arg)
        balance_hvms_vms(arg)
        return True

    return True

def balance_hvms_vms(arg):
    """ balance vms via number of vms """
    # get a list of vms on each hypervisor

    #run show()
    hypervisor_most_vms = get_most_vms_all()
    print str(type(hypervisor_most_vms)) + str(hypervisor_most_vms)
    hypervisor = hypervisor_most_vms

    # now list vms by mem

    pmem_cmdline = '/opt/vmm/pmem.all'
    print 'pmem_cmdline ' + str(pmem_cmdline)
    cmdline = pmem_cmdline
    runDict, e = run_cmd_dict(host=hypervisor, cmd=[cmdline], printout=False)
    outDict = {}
    for key in sorted(runDict.iterkeys()):
        stringBlob = runDict[key]
        lineList = stringBlob.split("\n")
        for line in lineList:
            if line:
                itemList = line.split()
                value = itemList[0]
                name = itemList[1]
                outDict[int(value)] = name


    print sorted(outDict.iterkeys())
    print 'lets do largest sorted is first item ' + str(sorted(outDict.iterkeys(), reverse=True)[0])
    largest_num = sorted(outDict.iterkeys(), reverse=True)[0]
    largest_name = outDict[largest_num]
    print 'largest_num ' + str(largest_num) + ' largest_name ' + str(largest_name)

    migration_vm = largest_name

    hvm_with_most_free_mem = get_hypervisor_most_free_mem()

    hvm_running_vm = get_hypervisor(migration_vm)

    print 'migrate ' + str(migration_vm) + ' to ' + str(hvm_with_most_free_mem)

    migrate_vm_explicit(migration_vm, source=hvm_running_vm, destination=hvm_with_most_free_mem)

    print 'DONE.balance_hvms_vms'
    return True

def balance_hvms_mem(arg):
    """ balance vms via mem """

    # get least available free mem hypervisor
    hypervisor_least_free_mem = get_hypervisor_least_free_mem()
    print 'least available free mem hypervisor ' + str(type(hypervisor_least_free_mem)) + ' ' + str(hypervisor_least_free_mem)

    # get a list or running vms,
    #which vm has most used mem
    #which vm has most use cpu

    print 'ok, getting vm with the most used mem on hypervisor ' + str(hypervisor_least_free_mem)
    #get running list of vms
    cmdline = 'virsh -q list --all'
    runDict, e = run_cmd_dict(host=hypervisor_least_free_mem, cmd=[cmdline], printout=False)

    runningList = []
    for key in sorted(runDict.iterkeys()):
        stringBlob = runDict[key]
        lineList = stringBlob.split("\n")
        for line in lineList:
            if line:
                itemList = line.split()
                name = itemList[1]
                state = itemList[2]
                runningList.append(name)

    print 'runningList ' + str(runningList)

    # get pcpu for each vm

    #vm = 'test-vm-01'
    #pmem
    #pid=$(/bin/ps -fu qemu | /bin/awk '{if ($10 == "'$vm'") print $2;}')
    #pmem_cmdline = "pid=$(/bin/ps -fu qemu | /bin/awk '{if ($10 == " + vm + ") print $2;}');"
    #pmem_cmdline += "/bin/ps --no-headers -p $pid -o rss"

    pmem_cmdline = ''
    #for name in runningList:
    #    pmem_cmdline += "pid=$(/bin/ps -fu qemu | /bin/awk '{if ($10 == " + str(name) + ") print $2;}');"
    #    pmem_cmdline += "/bin/ps --no-headers -p $pid -o rss"

    pmem_cmdline += '/opt/vmm/pmem.all'

    print 'pmem_cmdline ' + str(pmem_cmdline)
    cmdline = pmem_cmdline
    runDict, e = run_cmd_dict(host=hypervisor_least_free_mem, cmd=[cmdline], printout=False)
    outDict = {}
    for key in sorted(runDict.iterkeys()):
        stringBlob = runDict[key]
        lineList = stringBlob.split("\n")
        for line in lineList:
            if line:
                itemList = line.split()
                value = itemList[0]
                name = itemList[1]
                outDict[int(value)] = name


    print sorted(outDict.iterkeys())
    print 'lets do largest sorted is first item ' + str(sorted(outDict.iterkeys(), reverse=True)[0])
    largest_num = sorted(outDict.iterkeys(), reverse=True)[0]
    largest_name = outDict[largest_num]
    print 'largest_num ' + str(largest_num) + ' largest_name ' + str(largest_name)

    migratevm = largest_name

    hvm_with_most_free_mem = get_hypervisor_most_free_mem()

    #need source hypervisor
    sourcehypervisor = get_hypervisor(migratevm)
    if sourcehypervisor == "":
        print 'Error empty hypervisor balance_hvms_mem'
        return False

    print 'balance_hvms_mem source ' + str(sourcehypervisor) + ' migrate ' +  str(migratevm) + ' to ' + str(hvm_with_most_free_mem)

    migrate_vm_explicit(migratevm, source=sourcehypervisor, destination=hvm_with_most_free_mem)

    #print 'DIE.DIE balance_hvms'
    print 'Done.balance_hvms'
    #sys.exit(1)
    return True


def get_hw_mem_all(cli):
    """ get allocated hardware memory """
    #cmdline = "awk '/MemTotal/ {print $2}' /proc/meminfo"
    cmdline = cli
    #print str(type(cli)) + str(cli)
    hwmemDict, e = cmd_all([cmdline], printout=False)
    for key in sorted(hwmemDict.iterkeys()):
        print key, hwmemDict[key]
    return True

def get_vms_pmem(hypervisor):
    """ get the percentage of mem per vm on a hypervisor """
    #/opt/vmm/pmem.all
    # 845348 wwwmon-ca1d-01
    # 952172 dbmon-ca1d-01

    #print str(type(hypervisor)) + str(hypervisor)
    cmdline = "/opt/vmm/pmem.all"
    runDict, e = run_cmd_dict(host=hypervisor, cmd=[cmdline], printout=True)

    #print "DONE.get_vms_pmem"
    return True

def get_vms_pcpu(hypervisor):
    """ get the percentage of cpu per vm on a hypervisor """
    #/opt/vmm/pcpu.all
    # 35.82 wwwmon-ca1d-01
    # 19.82 dbmon-ca1d-01

    #print str(type(hypervisor)) + str(hypervisor)
    cmdline = "/opt/vmm/pcpu.all"
    runDict, e = run_cmd_dict(host=hypervisor, cmd=[cmdline], printout=True)

    return True

def show_hvm_cli(arg1):
    #./hvmsuper.py show most-vms
    #print str(type(arg1)) + str(arg1)
    #print 'here'
    argDict = {
       'most-vms' : get_most_vms_all,
       'least-vms' : find_least_vms_all,
       'most-mem' : find_least_mem_all,
       'hw-mem' : get_hw_mem_all,
       'mem-free' : get_hw_mem_all,
    }

    if arg1 and type(arg1) is list:
        arg1 = arg1[0]

    #print str(type(arg1)) + str(arg1)
    if arg1 == 'hw-mem':
        argDict[arg1]("awk '/MemTotal/ {print $2}' /proc/meminfo")
        return True
    if arg1 == 'mem-free':
        argDict[arg1]("awk '/MemFree/ {print $2}' /proc/meminfo")
        return True

    argDict[arg1]()
    return True

def run_alert(arg2=None, arg3=None):
    """ alerts... """
    """ cpu... """
    alert = arg2
    alert_option = arg3

    if arg2 and type(arg2) is list:
        #print 'arg2 list'
        arg2 = arg2[0]

    if arg3 and type(arg3) is list:
        #print 'arg3 list'
        arg3 = arg3[0]


    alerts = {
       'cpu' : run_cpu_alert_check,
    }

    if arg2:
        if arg2 in alerts.keys():
            alert = str(arg2)
        else:
            print 'Unknown/Not able to recognize: ' + str(arg2)
            return False

    if alert == 'cpu':
        run_cpu_alert_check()

    if alert is None:
        print ' alert is ' +  str(alert)
    return True

def run_cpu_alert_check():
    """ cpu all hypervisors """
    #print 'run_cpu_alert_check'

    cmdline = '/opt/vmm/pcpu.all'
    runDict, e = cmd_all(cmd=[cmdline], printout=False)
    outDict = {}

    #for key in sorted(runDict.iterkeys()):
    for key,val in runDict.items():
        stringBlob = runDict[key]
        lineList = stringBlob.split("\n")
        for line in lineList:
            if line:
                itemList = line.split()
                value = itemList[0]
                name = itemList[1]
                #outDict[int(value)] = name
                #outDict[float(value)] = name

                #outDict[name] = float(value)
                #outDict[name] = int_or_float(value)

                n = str(name) + ' on hypervisor ' + str(key)
                #outDict[n] = float(value)
                outDict[n] = int_float_none(value)

                #outDict[n] = float(str(value))
                #outDict[n] = str(value).isdigit()

    #print sorted(outDict.iterkeys())
    x = 100.00
    #print [outDict[i] for i in outDict if outDict[i] >= x]

    m = ''
    for k,v in outDict.items():
        if v >= x:
            print str(v) + ' ' + str(k)
            m += str(v) + ' ' + str(k) + ' \n'

    if sendEmail and m != '':
        msg = """The hypervisor supervisor has detected the following vms are over cpu threshhold : \n\r"""
        msg += str(m)
        msg += "\n\r Resolve: investigate system and running applications"
        sendMail(subject='detected high cpu', msg=msg)

    return True

def int_or_float(s):
    try:
        return int(s)
    except ValueError:
        return float(s)

def int_else_float(s):
    f = float(s)
    i = int(f)
    return i if i == f else f

def int_float_none(x):
    # it may be already int or float
    if isinstance(x, (int, float)):
        return x
    # all int like strings can be converted to float so int tries first
    try:
        return int(x)
    except (TypeError, ValueError):
        pass
    try:
        return float(x)
    except (TypeError, ValueError):
        return None



def gen_report(arg2=None, arg3=None):
    """ multiple report types """
    #[krink@mgmt-ca1d-01 hvm-super]$ ./hvmsuper.py gen-report cpu vmis-ca1d-10
    #[krink@mgmt-ca1d-01 hvm-super]$ ./hvmsuper.py gen-report vmis-ca1d-10 cpu

    host = None
    report = None

    if arg2 and type(arg2) is list:
        #print 'arg2 list'
        arg2 = arg2[0]

    if arg3 and type(arg3) is list:
        #print 'arg3 list'
        arg3 = arg3[0]

    #print 'Gen report ' + str(arg2) + ' ' + str(arg3)
    reports = {
       'cpu' : "grep -c ^processor /proc/cpuinfo;",
       'vcpu' : "virsh nodeinfo | grep ^'CPU(s):' | awk '{print $2}';",
       'hmem' : "virsh nodememstats | grep '^total' | awk '{print $3}';",
       'mem' : "free -g | grep ^Mem: | awk '{print $2}';free -g | grep ^Mem: | awk '{print $4}';/opt/vmm/mem.all.sum;/opt/vmm/pmem.all.sum;",
       'vms-pmem' : "/opt/vmm/pmem.all;",
       'vms-pcpu' : "/opt/vmm/pcpu.all;",
    }

    """ arg2 can be either host or report """
    if arg2:
        if arg2 in config.hosts:
            host = str(arg2)
        elif arg2 in reports.keys():
            report = str(arg2)
        else:
            print 'Not able to recognize: ' + str(arg2)
            return False

    """ arg3 can be either host or report """
    if arg3:
        if arg3 in config.hosts:
            host = str(arg3)
            report = str(arg2)
        elif arg3 in reports.keys():
            report = str(arg3)
        else:
            print 'Not able to recognize: ' + str(arg3)
            return False

    #print str(host) + ' ' + str(report)
    cmdline = reports.get(report)

    if host and report:
        run_cmd_dict(host=host, cmd=[cmdline])
        return True

    if report and not host:
        cmd_all([cmdline])
        return True

    bulkcmds = []
    for key in reports:
        #print key
        bulkcmds.append(reports[key])
    cmdline = ''.join(bulkcmds)
    #print str(bulkcmds)
    #print str(cmdline)

    if host and not report:
        run_cmd_dict(host=host, cmd=[cmdline])
        return True

    if not host and not report:
        cmd_all([cmdline])
        return True

    return True

def current_vms_state():
    """ gather current vms state """

    savefile = config.state['savefile']
    print 'savefile: ' + savefile

    #cmdline = '/opt/vmm/list'
    cmdline = 'virsh -q list --all'
    threads = [threading.Thread(target=sshCmdDict, args=(host,cmdline)) for host in config.hosts]
    for t in threads: t.start()
    for t in threads: t.join()

    if not os.path.isfile(savefile):
        #open(savefile, 'w').write(outputDict)
        json.dump(outputDict, open(savefile,'w'))
        print 'initialized savefile ' + str(savefile)
        return True

    with open(savefile) as lastfile:
        laststate = json.load(lastfile)

    currentfile = savefile + '.tmp'
    json.dump(outputDict, open(currentfile,'w'))
    with open(currentfile) as thisfile:
        thisstate = json.load(thisfile)

    a = thisstate
    b = laststate

    if a == b:
        print 'No diff'
        return True
    else:
        # a python dict w/ lists
        diff = []
        for key in sorted(a.iterkeys()):
            #print key
            if key in sorted(b.iterkeys()):
                # compare keys
                if a[key] != b[key]:
                    #print a[key],b[key]
                    print type(a[key]) #<type 'list'>
                    alist = a[key]
                    blist = b[key]
                    #print list(set(blist) - set(alist))
                    diff.append(list(set(blist) - set(alist)))
            elif key not in sorted(b.iterkeys()):
                diff.append('KeyNotFound ' + key)

            #print '---------------------------------'
        print 'diff ' + str(diff)

    #print 'end vms state'
    return True

def virsh_cmd(vm, hypervisor=None, virsh=None):
    """ virsh commands """
    """ run a virsh cmd on a hypervisor """

    if sys.argv[1]:
        virsh = sys.argv[1]

    if vm and type(vm) is list:
        vm = vm[0]

    if vm not in get_vm_xml_list():
        print 'vm xml not found: ' + str(vm)
        return False

    cmdline = 'virsh ' + str(virsh) + ' ' + str(vm)

    if hypervisor and type(hypervisor) is list:
        hypervisor = hypervisor[0]

    if not hypervisor:
        # try to get
        #print 'try to get'
        hypervisor = get_hypervisor(vm)
        if hypervisor == "":
            print 'Empty hypervisor virsh_cmd'
            return False

    print 'RUN ' + str(hypervisor) + ' cmd ' + str(cmdline)
    #output, err = run_cmd_dict(hypervisor, [cmdline], printout=True)
    run_cmd_dict(hypervisor, [cmdline], printout=True)
    return True

def evac_hvm_cli(src, dest=None):
    """ migrate all vms off this hvm """

    if src and type(src) is str and dest and type(dest) is list and len(dest) == 1:
        dest = dest[0]

    if src and type(src) is list and dest is None:
        src = src[0]

    special = None
    specials = [ 'up', 'down', 'auto']

    if not dest:
        special = 'auto'

    if dest and dest in specials:
        special = dest
        dest = None
        print 'special ' + special

    if src not in config.hosts:
        print 'src not found in config.hosts: ' + str(src)
        exitcode = 1
        return exitcode

    if not special and dest not in config.hosts:
        print 'dest not found in config.hosts: ' + str(dest)
        exitcode = 1
        return exitcode

    if src and special:
        print 'run special ' + str(special)
        print 'Need future method here... ' + str(special)
        exitcode = 0
        return exitcode

    if src and dest and not special:
        print 'evac ' + str(src) + ' to ' + str(dest)
        exitcode = evac_hvm_serial(src, dest)
        sys.exit(exitcode)
    else:
        print 'Error evac_hvm_cli unable to do... '
        exitcode = 99
        sys.exit(exitcode)

    sys.exit(exitcode)

def evac_hvm_serial(src, dest):
    exitcode = 99
    """ evacuate hvm to hvm
            move each vm serial in order """
    # get a list of runnig vms src hvm
    runningList = get_running_list_hvm(src)
    for vm in runningList:
        print vm
        #print 'evac_hvm_serial vm ' + str(vm) + ' src ' + str(src) + ' dest ' + str(dest)
        #migrate_vm_explicit(vm, source=src, destination=dest)
        #print 'migrate_vm_stdout vm ' + str(vm) + ' src ' + str(src) + ' dest ' + str(dest)
        exitcode = migrate_vm_stdout(vm, source=src, destination=dest)
        if (exitcode != 0):
            print 'Failed evac ' + str(exitcode) + ' ' + str(vm)
            continue

    print "evac_hvm_serial exitcode " + str(exitcode)
    return exitcode


def migrate_vm_cli(vm, source=None, destination=None):
    """ migrate_vm_cli """
    #print 'migrate_vm_cli'
    #print 'initial vm ' + str(type(vm)) + str(vm)
    #print 'initial source ' + str(type(source)) + str(source)
    #print 'initial dest ' + str(type(destination)) + str(destination)

    if vm and type(vm) is list:
        vm = vm[0]

    if vm not in get_vm_xml_list():
        print 'vm xml not found: ' + str(vm)
        exitcode = 1
        return exitcode

    if source and type(source) is list and len(source) == 2 and destination is None:
        sourceANDdestList = source
        source = sourceANDdestList[0]
        destination = sourceANDdestList[1]

    #print 'now.1 vm ' + str(type(vm)) + str(vm)
    #print 'now.1 source ' + str(type(source)) + str(source)
    #print 'now.1 dest ' + str(type(destination)) + str(destination)

    if source and type(source) is list and len(source) == 1 and destination is None:
        destination = source[0]
        source = None

    #print 'now.2 vm ' + str(type(vm)) + str(vm)
    #print 'now.2 source ' + str(type(source)) + str(source)
    #print 'now.2 dest ' + str(type(destination)) + str(destination)

    #source is empty, we need to figure where...
    if not source:
        #print 'try to get'
        source = get_hypervisor(vm)
        if source == "":
            print 'Unable to determin hypervisor'
            exitcode = 1
            return exitcode
    #print 'now.3 vm ' + str(type(vm)) + str(vm)
    #print 'now.3 source ' + str(type(source)) + str(source)
    #print 'now.3 dest ' + str(type(destination)) + str(destination)

    if not destination:
        #find best hypervisor
        print 'lets figure this out with a best... '
        hypervisor_least_vms = find_least_vms_all()
        if hypervisor_least_vms:
            destination = hypervisor_least_vms
            print 'using hvm_with_least_vms ' + str(type(hypervisor_least_vms)) + str(hypervisor_least_vms)


    #print 'final vm ' + str(type(vm)) + str(vm)
    #print 'final source ' + str(type(source)) + str(source)
    #print 'final dest ' + str(type(destination)) + str(destination)

    #migrate_vm_explicit(vm, source, destination)
    exitcode = migrate_vm_stdout(vm, source, destination)
    sys.exit(exitcode)
    #return exitcode

def migrate_vm_stdout(vm, source, destination):
    """ migrate a vm to new hypervisor """
    #print 'migrate_vm_stdout vm ' + str(vm) + ' source ' + str(source) + ' destination ' + str(destination)
    cmdline = 'virsh migrate --live --verbose ' + str(vm) + ' qemu+ssh://' + str(destination) + '/system'
    print str(source) + ' cmd ' + str(cmdline)
    exitcode = sshCmdStdOut(host=source,cmd=cmdline)
    return exitcode


def migrate_vm_explicit(vm, source=None, destination=None):
    """ migrate a vm to new hypervisor"""
    print 'migrate_vm_explicit vm ' + str(vm) + ' source ' + str(source) + ' destination ' + str(destination)
    cmdline = 'virsh migrate --live ' + str(vm) + ' qemu+ssh://' + str(destination) + '/system'
    print str(source) + ' cmd ' + str(cmdline)
    run_cmd_dict(source, [cmdline], printout=True)
    return True

def find_best_startup_hypervisor():
    hypervisor = None
    hypervisor_with_least_vms = get_least_vms_all()
    #print 'get_least_vms_all ' + str(type(hypervisor_with_least_vms)) + str(hypervisor_with_least_vms)
    hypervisor_with_most_free_mem = get_hypervisor_most_free_mem()

    print 'hypervisor_with_least_vms ' + str(type(hypervisor_with_least_vms)) + str(hypervisor_with_least_vms)
    print 'hypervisor_with_most_free_mem ' + str(type(hypervisor_with_most_free_mem)) + str(hypervisor_with_most_free_mem)

    #hypervisor = hypervisor_with_most_free_mem
    #print 'RETURN find_best_startup_hypervisor hypervisor_with_most_free_mem ' + str(hypervisor)

    hypervisor = hypervisor_with_least_vms
    print 'RETURN find_best_startup_hypervisor hypervisor_with_least_vms ' + str(hypervisor)
    return hypervisor

def start_not_running():
    """ start all not running """
    print 'working on start_not_running'
    # get list not running
    notrunningList = list_not_running(checkxml=False)

    print 'notrunningList ' + str(type(notrunningList)) + str(notrunningList)
    if notrunningList:
        for vm in notrunningList:
            print 'auto start up ' + vm
            start_vm(vm, hypervisor=None, printout=True)

    return True

def start_vm(vm, hypervisor=None, printout=True):
    """ start a vm on a hypervisor """

    if vm and type(vm) is list:
        vm = vm[0]

    if hypervisor and type(hypervisor) is list:
        hypervisor = hypervisor[0]

    #print str(type(vm)) + str(vm)
    #print str(type(hypervisor)) + str(hypervisor)

    # get valid xml
    xmlList = get_vm_xml_list()
    if vm not in xmlList:
        if printout: print 'vm xml not found: ' + str(vm)
        return False

    #verify not running
    isRunningDict = is_running_all(vm, printout=False)
    if isRunningDict:
        print 'Already running ' + vm + ' on ' + str(isRunningDict.keys())
        return False

    #xmlFiles = glob.glob("xml/*.xml")
    #xmlFile = "xml/" + vm + ".xml"
    xmlFile = config.xml['location'] + vm + ".xml"

    if not hypervisor:
        print 'Getting with find_best_startup_hypervisor'
        hypervisor = find_best_startup_hypervisor()

    cmdline = 'virsh create ' + str(xmlFile)
    print str(hypervisor) + ' cmd ' + str(cmdline)
    run_cmd_dict(hypervisor, [cmdline], printout=True)

    #print 'start_vm'
    return True


def get_vm_xml_list():
    """ return an xml list """
    xmlList = []
    xmlFiles = glob.glob("xml/*.xml")
    for file in xmlFiles:
        file = os.path.basename(file)
        xmlList.append(os.path.splitext(file)[0])
    return xmlList

def get_hypervisor(vm):
    """ return string of only one """
    hypervisor=''
    hypervisorDict = is_running_all(vm, printout=True)
    #print type(hypervisorDict)
    for key in sorted(hypervisorDict.iterkeys()):
        hypervisor = key
    return hypervisor

def get_running_instances_all():
    """ return a dict """
    cmdline = "virsh -q list --all "
    returnDict, e = cmd_all([cmdline], printout=False)
    return returnDict, e

def get_running_instances_hvm(hypervisor):
    """ return a dict """
    cmdline = "virsh -q list --all "
    returnDict, e = run_cmd_dict(host=hypervisor, cmd=[cmdline], printout=False)
    return returnDict, e

def get_running_list_hvm(hypervisor):
    """ return a list """
    runningList = []
    runningDict, e = get_running_instances_hvm(hypervisor)
    for key in sorted(runningDict.iterkeys()):
        stringBlob = runningDict[key]
        lineList = stringBlob.split("\n")
        #print str(lineList)
        for line in lineList:
            if line:
                itemList = line.split()
                name = itemList[1]
                state = itemList[2]
                runningList.append(name)

    #print str(runningList)
    return runningList

def get_running_list_all():
    """ return a list """
    runningList = []
    runningDict, e = get_running_instances_all()
    for key in sorted(runningDict.iterkeys()):
        stringBlob = runningDict[key]
        lineList = stringBlob.split("\n")
        #print str(lineList)
        for line in lineList:
            if line:
                itemList = line.split()
                name = itemList[1]
                state = itemList[2]
                runningList.append(name)

    #print str(runningList)
    return runningList

def get_most_vms_all():
    print 'find most populated hypervisor'
    hvmDict = get_hypervisor_vms_num()
    #print str(hvmDict)
    #sys.exit(1)

    for key in sorted(hvmDict.iterkeys()):
        print key, hvmDict[key]

    #print max(hvmDict, key=hvmDict.get)
    hvm_with_most_vms = max(hvmDict, key=hvmDict.get)
    print 'hvm_with_most_vms ' + str(hvm_with_most_vms) + ' ' + str(hvmDict[hvm_with_most_vms])

    hvm_with_least_vms = min(hvmDict, key=hvmDict.get)
    print 'hvm_with_least_vms ' + str(hvm_with_least_vms) + ' ' + str(hvmDict[hvm_with_least_vms])

    #hypervisor = str(hvm_with_least_vms)
    hypervisor = hvm_with_most_vms
    print 'RETURN hvm_with_most_vms ' + str(type(hypervisor)) + str(hypervisor)
    return hypervisor

def get_least_vms_all():
    print 'find least populated hypervisor'
    hvmDict = get_hypervisor_vms_num()

    for key in sorted(hvmDict.iterkeys()):
        print key, hvmDict[key]

    hvm_with_most_vms = max(hvmDict, key=hvmDict.get)
    print 'hvm_with_most_vms ' + str(hvm_with_most_vms) + ' ' + str(hvmDict[hvm_with_most_vms])

    hvm_with_least_vms = min(hvmDict, key=hvmDict.get)
    print 'hvm_with_least_vms ' + str(hvm_with_least_vms) + ' ' + str(hvmDict[hvm_with_least_vms])

    hypervisor = hvm_with_least_vms
    print 'RETURN hvm_with_least_vms ' + str(type(hypervisor)) + str(hypervisor)
    return hypervisor


def find_least_vms_all():
    print 'find least populated hypervisor'
    hvmDict = get_hypervisor_vms_num()
    #print str(hvmDict)
    #sys.exit(1)

    for key in sorted(hvmDict.iterkeys()):
        print key, hvmDict[key]

    #print max(hvmDict, key=hvmDict.get)
    hvm_with_most_vms = max(hvmDict, key=hvmDict.get)
    print 'hvm_with_most_vms ' + str(hvm_with_most_vms) + ' ' + str(hvmDict[hvm_with_most_vms])

    hvm_with_least_vms = min(hvmDict, key=hvmDict.get)
    print 'hvm_with_least_vms ' + str(hvm_with_least_vms) + ' ' + str(hvmDict[hvm_with_least_vms])

    #print 'DIE.DIE'
    #sys.exit(1)

    #hypervisor = str(hvm_with_least_vms)
    hypervisor = hvm_with_least_vms
    print 'RETURN hvm_with_least_vms ' + str(type(hypervisor)) + str(hypervisor)
    return hypervisor

def find_most_mem():
    hypervisor=None
    print 'find most mem free hypervisor'
    hypervisor = get_hypervisor_most_free_mem()
    return hypervisor


def get_hypervisor_most_free_mem():
    """ return sting of one """
    hypervisor=None

    cmdline = "awk '/MemFree/ {print $2}' /proc/meminfo"
    # get a running dict, which is one big dict of host:stringBlog
    hvmemDict, e = cmd_all([cmdline], printout=False)
    for key in sorted(hvmemDict.iterkeys()):
        value = hvmemDict[key]
        print 'hvmemDict ' + str(key) + str(type(value)) + str(value)

    hvm_with_most_free_mem = min(hvmemDict, key=hvmemDict.get)
    hvm_with_least_free_mem = max(hvmemDict, key=hvmemDict.get)

    print 'hvm_with_most_free_mem ' + str(hvm_with_most_free_mem) + ' ' + str(hvmemDict[hvm_with_most_free_mem])

    hypervisor = hvm_with_most_free_mem
    #print str(type(hypervisor)) + str(hypervisor)
    #print 'DIE.DIE get_hypervisor_most_free_mem'
    #sys.exit(1)
    return hypervisor

def get_hypervisor_least_free_mem():
    """ return sting of one """
    hypervisor=None

    cmdline = "awk '/MemFree/ {print $2}' /proc/meminfo"
    # get a running dict, which is one big dict of host:stringBlog
    thisDict, e = cmd_all([cmdline], printout=False)
    for key in sorted(thisDict.iterkeys()):
        value = int(thisDict[key])
        print 'thisDict ' + str(key) + str(type(value)) + str(value)

    hvm_with_most_free_mem = min(thisDict, key=thisDict.get)
    hvm_with_least_free_mem = max(thisDict, key=thisDict.get)

    print 'hvm_with_most_free_mem via mini ' + str(hvm_with_most_free_mem) + ' ' + str(thisDict[hvm_with_most_free_mem])
    print 'hvm_with_least_free_mem via max ' + str(hvm_with_least_free_mem) + ' ' + str(thisDict[hvm_with_least_free_mem])

    #hvm_with_least_free_mem_lambda = max(thisDict.items(), key=lambda x: x[1])[0]
    #print 'hvm_with_least_free_mem_lambda ' + str(hvm_with_least_free_mem_lambda) + ' ' + str(thisDict[hvm_with_least_free_mem_lambda])

    #min_value = min(thisDict.values())
    #result = [key for key, value in thisDict.iteritems() if value == min_value]
    #print 'results in ' + str(result)
    #min_val = max(thisDict.itervalues())
    #print [k for k, v in thisDict.iteritems() if v == min_val]

    reverseDict = {}
    for key in sorted(thisDict.iterkeys()):
        reverseDict[int(thisDict[key])] = key

    #print str(reverseDict)
    print sorted(reverseDict.iterkeys())
    print 'lets do smallest sorted is first item ' + str(sorted(reverseDict.iterkeys())[0])
    smallest_num = sorted(reverseDict.iterkeys())[0]
    smallest_name = reverseDict[smallest_num]
    print 'smallest_num ' + str(smallest_num) + ' smallest_name ' + str(smallest_name)

    hypervisor = smallest_name
    return hypervisor

def get_hvm_cli(hypervisor, item):
    """
        get <hypervisor> [most-mem-used|most-mem-defined
                          most-cpu-used|most-cpu-defined]

                          - most-mem - highest memory used
                          - most-mem-defined - largest memory defined
                          - most-cpu - highest cpu utilization
                          - most-cpu-defined - largest cpu defined

    """
    #validate hypervisor
    if not hypervisor in config.hosts:
        print 'hypervisor not found: ' + str(hypervisor)
        return False

    if type(item) is list:
        item = item[0]

    items = {
      'most-mem-used' : get_vm_mem_used,
      'least-mem-used' : get_vm_mem_used,
      'most-mem-defined' : get_vm_mem_defined,
      'least-mem-defined' : get_vm_mem_defined,
    }
    try:
        items[item](hypervisor, option=item)
    except KeyError as e:
        print 'Unknown: ' + str(e)
        return False
    return True


def get_vm_mem_used(hypervisor, option=None):
    """ get_vm_most_mem_used """
    #/opt/vmm/pmem
    #pid=$(/bin/ps -fu qemu | /bin/awk '{if ($10 == "'$vm'") print $2;}')
    #/bin/ps --no-headers -p $pid -o rss
    vm=None
    print "get_vm_most_mem_used "
    print 'hypervisor ' + hypervisor + ' option ' + option

    cmdline = "for vm in $(virsh -q list --all | awk '{print $2}');"
    cmdline += "do echo -n $vm; echo -n \" \"; "
    cmdline += "pid=$(/bin/ps -fu qemu | /bin/awk '{if ($10 == \"'$vm'\") print $2}');"
    cmdline += "/bin/ps --no-headers -p $pid -o rss;"
    cmdline += "done"

    outDict = {}
    out,e = run_cmd_dict(hypervisor, [cmdline], printout=False)
    #print str(type(out)) +str(out)
    for key in out.iterkeys():
        stringBlob = out[key]
        #print str(stringBlob)
        lineList = stringBlob.split("\n")
        for line in lineList:
            if line:
                itemList = line.split()
                #print str(itemList)
                name = itemList[0]
                size = itemList[1]
                print name + ' ' + size
                outDict[name] = int(size)

    largest_num = max(outDict, key=outDict.get)
    smallest_num = min(outDict, key=outDict.get)
    print 'largest_vm_mem ' + str(largest_num) + ' ' + str(outDict[largest_num])
    print 'smallest_vm_mem ' + str(smallest_num) + ' ' + str(outDict[smallest_num])

    if option == 'most-mem-used':
        vm = largest_num

    if option == 'least-mem-used':
        vm = smallest_num

    print 'RETURN ' + str(option) + ' ' + str(vm) + ' ' + str(outDict[vm])
    return vm

def get_vm_mem_defined(hypervisor, option=None):
    """ get_vm_most_mem_defined """
    #/opt/vmm/mem
    #virsh dominfo "$vm" | grep '^Max memory:' | awk '{print $3 " " $4}'
    vm=None
    print "get_vm_most_mem_defined "
    print 'hypervisor ' + hypervisor + ' option ' + option

    cmdline = "for vm in $(virsh -q list --all | awk '{print $2}');"
    cmdline += "do echo -n $vm; echo -n \" \"; "
    cmdline += "virsh dominfo \"$vm\" | grep '^Max memory:' | awk '{print $3}';"
    cmdline += "done"

    outDict = {}
    out,e = run_cmd_dict(hypervisor, [cmdline], printout=False)
    #print str(type(out)) +str(out)
    for key in out.iterkeys():
        stringBlob = out[key]
        #print str(stringBlob)
        lineList = stringBlob.split("\n")
        for line in lineList:
            if line:
                itemList = line.split()
                #print str(itemList)
                name = itemList[0]
                size = itemList[1]
                print name + ' ' + size
                outDict[name] = int(size)

    largest_num = max(outDict, key=outDict.get)
    smallest_num = min(outDict, key=outDict.get)
    print 'largest_vm_mem ' + str(largest_num) + ' ' + str(outDict[largest_num])
    print 'smallest_vm_mem ' + str(smallest_num) + ' ' + str(outDict[smallest_num])

    if option == 'most-mem-defined':
        vm = largest_num

    if option == 'least-mem-defined':
        vm = smallest_num

    print 'RETURN ' + str(option) + ' ' + str(vm) + ' ' + str(outDict[vm])
    return vm


def get_hypervisor_vms_num():
    """ return hypervisor vms number
        as a dict host:num """

    returnDict = {}
    # get a running dict, which is one big dict of host:stringBlog
    runningDict, e = get_running_instances_all()

    for key in sorted(runningDict.iterkeys()):
        stringBlob = runningDict[key]
        lineList = stringBlob.split("\n")
        #print str(lineList)
        count = 0
        for line in lineList:
            if not line:
                returnDict[key] = count
            else:
                itemList = line.split()
                name = itemList[1]
                state = itemList[2]
                count += 1
                returnDict[key] = count

    return returnDict

def find_least_mem_all():
    """ return string hypervisor """
    hypervisor=None
    print 'DIE.DIE find_least_mem_all'
    sys.exit(1)
    return hypervisor

def find_least_cpu():
    """ return string hypervisor """
    hypervisor=None
    return hypervisor

def find_best_hypervisor():
    """ return string hypervisor """
    hypervisor=None

    hypervisor_least_vms = find_least_vms_all()
    if hypervisor_least_vms:
        hypervisor = hypervisor_least_vms
        print 'using hvm_with_least_vms ' + str(type(hypervisor)) + str(hypervisor)

    hypervisor_most_mem = find_most_mem()
    if hypervisor_most_mem:
        hypervisor = hypervisor_most_mem
        print 'using hypervisor_most_mem ' + str(type(hypervisor)) + str(hypervisor)



    print 'Going with hypervisor ' + str(type(hypervisor)) + str(hypervisor)
    #print 'DIE.DIE find_best_hypervisor'
    #sys.exit(1)
    """ there can only be one in the end """
    return hypervisor


def check_dup_mac(vm=None):
    """ check if running more than one mac addr """
    print 'Need future dup mac'
    return True

def check_dup_uuid(vm=None):
    """ check if running more than one uuid addr """
    print 'Need future dup uuid'
    return True

def check_dup_lun(vm=None):
    """ check if running more than one lun being used """
    print 'Need future dup lun'
    return True

def check_dup_vm():
    """ check if running more than one instance """
    #print 'get a list of all running vms'
    runningList = get_running_list_all()
    dupDict = {}
    dupes = [x for n, x in enumerate(runningList) if x in runningList[:n]]
    if dupes:
        print 'dups found: ' + str(dupes)
        for dup in dupes:
            dupDict = is_running_all(dup, printout=True)

        keyList = []
        for key in sorted(dupDict.iterkeys()):
            #print 'key ' + key
            keyList.append(key)

        if sendEmail:
            msg = """The hypervisor supervisor has detected the following vms are running dups: \n\r"""
            msg += "\n".join(dupes)
            msg += "\n running on: "
            #msg += "\n".join(keyList)
            msg += str(keyList)
            msg += "\n\r Solution: destroy all instances immediately"
            sendMail(subject='detected dup instances', msg=msg)
            #print str(msg)

    return dupDict

def check_cmd(argv=None):
    #check [dupmac|dupuuid|duplun|dupvm]
    #checkoptionsList = [ dupmac, dupuuid, duplun, dupvm ]

    #print type(argv)
    if type(argv) is list:
        argv = argv[0]

    run=None
    checkcmd = {
      'dupmac' : check_dup_mac,
      'dupuuid' : check_dup_uuid,
      'duplun' : check_dup_lun,
      'dupvm' : check_dup_vm,
    }
    #options['check-cmd'](option=sys.argv[2])
    #print str(option)
    try:
        run = checkcmd[argv]()
    except KeyError as e:
        print str(e)

    return run

#check_ssh_cli
def check_ssh_cli(hypervisor):
    exitcode = 99
    hypervisor = hypervisor[0]
    if hypervisor not in config.hosts:
        print "hypervisor not found: " + str(hypervisor)
        exitcode = 1
        return exitcode
    exitcode = check_hvm_ssh(hypervisor)
    sys.exit(exitcode)

def check_ssh_blocking(hypervisor):
    exitcode = 99
    print 'check_ssh_blocking '

    print 'hypervisor ' + str(type(hypervisor)) + str(hypervisor)

    #hypervisor = hypervisor[0]
    #hypervisor is a string here

    # or a list apparently...
    #hypervisor <type 'list'>['vmis-ca1d-01']
    #hypervisor not found: ['vmis-ca1d-01']
    if type(hypervisor) is list:
        hypervisor = hypervisor[0]

    if hypervisor not in config.hosts:
        print "hypervisor not found: " + str(hypervisor)
        exitcode = 1
        return exitcode

    TermSig = None
    while TermSig is None:
        exitcode = check_hvm_ssh(hypervisor)
        print str(exitcode)
        if exitcode == 0:
            TermSig = exitcode
        else:
            print 'check again in 3...'
            time.sleep(3)

    print 'DONE.check_ssh_blocking ' + str(hypervisor)
    return exitcode

def status_cli(arg1=None):
    """ status_cli  """

    hypervisor = None
    vm = None

    if not arg1:
        print 'Need future method here.  no arg1 status_cli ' + str(arg1)
        return True

    if arg1 and type(arg1) is list:
        arg1 = arg1[0]
    else:
        arg1 = 'Unset'

    if arg1 in config.hosts:
        hypervisor = arg1

    if hypervisor:
        print 'Need future method here.  hypervisor specific  status_cli ' + str(hypervisor)
        return True

    xmlList = get_vm_xml_list()
    if arg1 not in xmlList:
        #print 'vm xml not found: ' + str(arg1)
        print 'not found: ' + str(arg1)
        return False

    runDict = is_running_all(arg1, printout=True)
    if runDict:
        #print 'yes, true if it exists'
        return True

    return True

def is_running_all(vm=None, printout=True):
    #check if vm is running
    """" isrunning|whohas|status
         return empty returnDict if nothing else """

    #print 'vm: ' + str(vm) + ' ' + str(printout)
    returnDict = {}

    if type(vm) is list:
        vm = vm[0]

    #this is double checking...
    xmlList = get_vm_xml_list()
    if vm not in xmlList:
        if printout: print 'vm xml not found: ' + str(vm)
        return returnDict

    runningDict, e = get_running_instances_all()

    count = 0
    for key in sorted(runningDict.iterkeys()):
        stringBlob = runningDict[key]
        lineList = stringBlob.split("\n")
        #print str(lineList)
        for line in lineList:
            if line:
                itemList = line.split()
                name = itemList[1]
                state = itemList[2]
                if vm == name:
                    count += 1
                    if printout: print 'RUNNING ' + vm + ' on hypervisor ' + key + ' state ' + state
                    returnDict[key] = [vm, state]
    if count == 0:
        if printout: print 'NOT-RUNNING ' + vm
        return returnDict

    # when the count is more than one, we have a dup runner!
    #print str(returnDict)
    return returnDict

class threadWithReturn(threading.Thread):
    def __init__(self, *args, **kwargs):
        super(threadWithReturn, self).__init__(*args, **kwargs)
        self._return = None

    def run(self):
        if self._Thread__target is not None:
            self._return = self._Thread__target(*self._Thread__args, **self._Thread__kwargs)

    def join(self, *args, **kwargs):
        super(threadWithReturn, self).join(*args, **kwargs)
        return self._return


if __name__ == "__main__":

    options = {
      'list' : list_vms,
      'list-vms' : list_vms_serial,
      'list-not-running' : list_not_running,
      'list-no-xml' : list_not_running,
      'list-hypervisors' : list_hypervisors,
      'run-cmd' : run_cmd_cli,
      'cmdall' : cmd_all,
      'report' : gen_report,
      'gen-balance-report' : gen_balance_report,
      'check' : check_cmd,
      'check-ssh' : check_ssh_cli,
      'check-ssh-blocking' : check_ssh_blocking,
      'status' : status_cli,
      'migrate' : migrate_vm_cli,
      'start' : start_vm,
      'start-not-running' : start_not_running,
      'destroy' : virsh_cmd,
      'shutdown' : virsh_cmd,
      'state' : current_vms_state,
      'evac' : evac_hvm_cli,
      'show' : show_hvm_cli,
      'balance' : balance_hvms,
      'get' : get_hvm_cli,
      'maintenance' : maintenance_hvm_cli,
      'alert' : run_alert,
    }

    if len(sys.argv) < 1:
        usage()
        sys.exit(0)

    if sys.argv[1:]:

        if sys.argv[1] in config.hosts:
            options['run-cmd'](host=sys.argv[1], cmd=sys.argv[2:])
            sys.exit(0)

        if sys.argv[1] == "cmdall":
            options[sys.argv[1]](cmd=sys.argv[2:])
            sys.exit(0)

        if sys.argv[1] in options.keys():
            if len(sys.argv) >= 4:
                options[sys.argv[1]](sys.argv[2], sys.argv[3:])
                sys.exit(0)
            if len(sys.argv) == 3:
                options[sys.argv[1]](sys.argv[2:])
                sys.exit(0)
            else:
                options[sys.argv[1]]()
                sys.exit(0)

        if sys.argv[1] not in options.keys():
            print 'Unknown Option: ' + sys.argv[1]
            usage()
            sys.exit(0)
    else:
        usage()
        sys.exit(0)


sys.exit(1)

# need resume...
#[root@vmis-ca1d-01 ~]# virsh resume archive-ca1d-01
#Domain archive-ca1d-01 resumed

# TODO
# logging, log handle
# daemonize
# running pid file

# FIX.THIS.1.2
#vms that are defined, but not running....
#
#[krink@mgmt-ca4-01 vmis1-ca4]$ ./hvmsuper.py list
#vmis1-ca4-01
# -     logs-ca4-04                    shut off
#
#vmis1-ca4-02
# 1     lockbox-ca4-01                 running
# 2     ldap-ca4-02                    running
# 3     db-ca4-07                      running
# 4     lbc4-ca4-02                    running
#


